# Contributing

## Prerequisites

* [Nix](https://nixos.org/download) with
  [Flakes](https://nixos.wiki/wiki/Flakes) enabled.
  * If using Nix, you only need the above to build this project.

If you are not willing to use Nix, this project requires:

* [rust](https://www.rust-lang.org/) 1.69.0+

## Building

    cargo build

## Local Execution

TODO. It is possible to locally test, but you will need to set up a
custom OIDC provider in your AWS account and find an OIDC provider
that you can pass sample credentials using to emulate the service.
