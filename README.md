# aws-gitlab-service

A GitLab CI service for granular AWS authentication controls
with simple configuration controls.

GitLab CI has the capability of managing AWS credentials via [OpenID
connect](https://docs.gitlab.com/ee/ci/cloud_services/aws/), which is
significantly better for permissions over IAM Users (see the FAQ) --
but the STS command to authenticate is long and needs to be repeated
in all your projects that use AWS.

This service simplifies the process of using OpenID for AWS
authentication in your pipelines. It simulates [IMDSv2 EC2 metadata
service](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-metadata.html)
that many AWS services use to provide seamless authentication. The
simulated service provides several benefits:

1. Unlike environment-variable or file-based authentication, the EC2
   metadata service provides refreshable credentials. When AWS
   credentials expire, it will automatically poll our auth service for
   refreshed credentials.
2. Nearly every AWS tool and SDK supports the EC2 metadata service,
   ensuring that you will have long-lasting support.
3. The service itself runs as a sidecar to your main CI job, meaning
   that you do not need to run any additional setup steps inside your
   jobs or install any special tooling.

## Usage

### AWS Configuration

You will need to start by creating an identity provider and a role in
your AWS account.

Start by navigating to the `Identity providers` tab in the `AWS IAM`
console. Click "Add Provider" in the top right corner and then fill in
the following information:

* `Provider Type`: OpenID Connect
* `Provider URL`: The root URL of your GitLab instance. For example, `https://gitlab.com`
* `Provider Audience`: This can be anything, but by default we recommend `https://gitlab.com`.

Create an IAM role named
`gitlab-ci-${YOUR_GROUP_NAME}-${YOUR_PROJECT_NAME}` with the following
trust relationship:

    {
        "Version": "2012-10-17",
        "Statement": [
            {
                "Effect": "Allow",
                "Principal": {
                    "Federated": "arn:aws:iam::${YOUR_AWS_ACCOUNT_ID}:oidc-provider/${YOUR_PROVIDER_AUDIENCE}"
                },
                "Action": "sts:AssumeRoleWithWebIdentity",
                "Condition": {
                    "StringLike": {
                        "gitlab.com:sub": "project_path:${YOUR_GROUP_NAME}/${YOUR_PROJECT_NAME}:ref_type:branch:ref:*"
                    }
                }
            }
        ]
    }

This role will allow CI from any branch of your pipeline to use AWS
authentication. In order to further control authentication by branch,
you may also replace `*` with a specific branch name and change
`StringLike` to `StringEquals`.

### GitLab CI Configuration

For any new CI pipelines, add the following:

    default:
      id_tokens:
        GITLAB_OIDC_TOKEN_AUTH:
          # Generally the URL of your GitLab instance
          aud: https://gitlab.example.com
      variables:
        # The AWS account we want to connect to
        AWS_ACCOUNT_ID: "your-aws-account-id"
        # Tells the AWS tooling to grab credentials from our service
        AWS_EC2_METADATA_SERVICE_ENDPOINT: "http://aws-gitlab-service:6969/"
        # The default region to use. By default it will be us-east-1
        AWS_DEFAULT_REGION: "us-east-1"
        # This is an alias since by default, id_tokens are not passed to services
        GITLAB_OIDC_TOKEN: "$GITLAB_OIDC_TOKEN_AUTH"
        # This is the same audience configured above and
        # in GITLAB_OIDC_TOKEN_AUTH
        GITLAB_AUDIENCE: ${YOUR_PROVIDER_AUDIENCE_FROM_ABOVE}
        # Optional comma-separated list of branches with special roles. Useful
        # if you want only your main branch to have permissions to write to
        # prod.
        AWS_ROLE_NAME_INCLUDE_BRANCH: "main"
      services:
        - name: registry.gitlab.com/chadac/aws-gitlab-service:latest
          alias: aws-gitlab-service

You can now run any regular AWS commands from your pipelines without needing
to perform any additional authentications.

### Role Naming

By default, `aws-gitlab-service` will infer the role name as
`arn:aws:iam::$AWS_ACCOUNT_ID:role/gitlab-ci-${CI_PROJECT_NAMESPACE}-${CI_PROJECT_NAME}`
for a project hosted at `https://gitlab.com/CI_PROJECT_NAMESPACE/CI_PROJECT_NAME`.

You may also specify special suffixes to avoid needing to include the
full name in your role specification. For example, with
`AWS_ROLE_NAME_SUFFIX: "main"`, CI pipelines that run on the `main`
branch of your repository would assume the IAM role
`arn:aws:iam::$AWS_ACCOUNT_ID:role/gitlab-ci-${CI_PROJECT_NAMESPACE}-${CI_PROJECT_NAME}-main`.

## FAQ

### Why OIDC?

OIDC has two distinct advantages over the most common method of using
CI secrets + IAM users for permissions management:

1. **Less manual work**: For organizations that manage hundreds of
   repositories, this service provides a low-configuration mechanism
   for handling granular repository permissions. Setting up
   authentication in the future can be easily automated in pure AWS
   with simple Terraform-based templates.
2. **Trusted Identities**: GitLab runners provision OIDC identities in
   a controlled context that cannot be easily faked. Thus, it allows
   administrators to properly enforce controls based on the specific
   GitLab project being run without needing to manually configure
   per-project or per-project+branch credentials.
3. **Short-lived credentials**: IAM role credentials only live for 15
   minutes usually, and 12 hours maximum. There is a significantly
   lower risk of exploitation when credentials are leaked.

### Why not just env variables?


## Contributing

TODO
