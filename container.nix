{
  aws-gitlab-service,
  cacert,
  dockerTools,
}:
dockerTools.buildLayeredImage {
  name = "aws-gitlab-service";
  tag = "latest";

  config = {
    Cmd = [ "${aws-gitlab-service}/bin/aws-gitlab-service" ];
    Env = [
      "SSL_CERT_FILE=${cacert}/etc/ssl/certs/ca-bundle.crt"
    ];
    ExposedPorts = {
      "6969/tcp" = {};
    };
  };
}
