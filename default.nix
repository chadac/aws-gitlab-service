{
  lib,
  perl,
  pkg-config,
  rustPlatform,
  symlinkJoin,
  openssl,
}:
let
  libssl = symlinkJoin {
    name = "libssl-merged";
    paths = [ openssl.out openssl.dev ];
  };
in
rustPlatform.buildRustPackage rec {
  pname = "aws-gitlab-service";
  version = "1.0.0";

  src = lib.cleanSource ./.;
  # waiting for https://github.com/NixOS/nixpkgs/pull/255025
  # src = lib.fileset.toSource {
  #   root = ./.;
  #   fileset = lib.fileset.union
  #     ./Cargo.toml
  #     ./src;
  # };

  nativeBuildInputs = [ pkg-config ];

  cargoLock = {
    lockFile = ./Cargo.lock;
  };

  OPENSSL_SRC_PERL = "${perl}/bin/perl";
  OPENSSL_DIR = "${libssl}";

  meta = with lib; {
    description = "A lightweight EC2 metadata service for AWS auth in GitLab CI.";
    homepage = "https://gitlab.com/chadac/aws-gitlab-service";
    license = licenses.mit;
    maintainers = [];
  };
}
