#![allow(unreachable_code)]
use chrono::{TimeZone, Utc};
use rouille::router;
use serde::Serialize;
use std::env;

mod sts;

#[derive(Serialize)]
#[allow(non_snake_case)]
struct Creds {
    pub Code: String,
    pub Type: String,
    pub AccessKeyId: String,
    pub SecretAccessKey: String,
    pub Token: String,
    pub Expiration: String,
    pub LastUpdated: String,
}

fn convert_timestamp(timestamp: f32) -> String {
    let datetime = Utc.timestamp_opt(timestamp as i64, 0).unwrap();
    datetime.format("%Y-%m-%dT%H:%M:%SZ").to_string()
}

fn main() {
    let host = env::var("SERVER_HOST").unwrap_or("0.0.0.0".to_string());
    let port = env::var("SERVER_PORT").unwrap_or("6969".to_string());

    let oidc_token = env::var("GITLAB_OIDC_TOKEN").expect("GITLAB_OIDC_TOKEN must be set!");

    let group = env::var("CI_PROJECT_NAMESPACE")
        .expect("CI_PROJECT_NAMESPACE must be set (provided by GitLab CI)");
    let project =
        env::var("CI_PROJECT_NAME").expect("CI_PROJECT_NAME must be set (provided by GitLab CI)");
    let branch =
        env::var("CI_COMMIT_BRANCH").expect("CI_COMMIT_BRANCH must be set (provided by GitLab CI)");
    let suffix = env::var("AWS_ROLE_NAME_SUFFIX");

    let aws_account = env::var("AWS_ACCOUNT_ID").expect("AWS_ACCOUNT_ID must be set!");
    let aws_partition = env::var("AWS_PARTITION").unwrap_or("aws".to_string());
    let aws_role_name =
        env::var("AWS_ROLE_NAME").unwrap_or({
            match &suffix {
                Ok(sfx) =>
                    format!("gitlab-ci-{}-{}-{}", group, project, sfx).replace("/", "-"),
                Err(_) =>
                    format!("gitlab-ci-{}-{}", group, project).replace("/", "-"),
            }
        });
    let aws_region = env::var("AWS_DEFAULT_REGION").unwrap_or("us-east-1".to_string());
    let aws_role_arn = match env::var("AWS_ROLE_ARN") {
        Ok(role_arn) => role_arn,
        _ => {
            format!(
                "arn:{}:iam::{}:role/{}",
                aws_partition, aws_account, aws_role_name
            )
        }
    };

    let aws_role_session_name =
        env::var("AWS_ROLE_SESSION_NAME").unwrap_or(
            format!("gitlab-ci-{}-{}", group, project).replace("/", "-")
        );

    run_server(
        host,
        port,
        oidc_token,
        aws_role_session_name,
        aws_role_arn,
        aws_region
    );
}

fn parse_aws_role_name(
    aws_role_arn: &str,
) -> String {
    let parts = aws_role_arn.split(":");
    parts.last().unwrap().split("/").last().unwrap().to_string()
}

fn run_server(
    host: String,
    port: String,
    oidc_token: String,
    aws_role_session_name: String,
    aws_role_arn: String,
    aws_region: String
) {
    let server = format!("{}:{}", host, port);
    let aws_role_name = parse_aws_role_name(&aws_role_arn);
    println!("Now listening on {}", server);

    rouille::start_server(server, move |request| {
        router!(request,
            (GET) (/) => {
                rouille::Response::text("Healthy")
            },
            (PUT) (/latest/api/token) => {
                rouille::Response::text("EMPTY_TOKEN_RESPONSE")
            },
            (GET) (/latest/meta-data/iam/security-credentials/) => {
                rouille::Response::text(aws_role_name.to_string())
            },
            (GET) (/latest/meta-data/placement/availability-zone/) => {
                // Used for inferring the AWS region
                rouille::Response::text(format!("{}a", aws_region))
            },
            (GET) (/latest/meta-data/iam/security-credentials/{_ignore: String}) => {
                let resp = sts::assume_role_with_web_identity(
                    &aws_role_arn,
                    &oidc_token,
                    &aws_role_session_name,
                    None
                ).unwrap();
                let creds = Creds {
                    Code: "Success".to_string(),
                    Type: "AWS-HMAC".to_string(),
                    AccessKeyId: resp.AccessKeyId,
                    SecretAccessKey: resp.SecretAccessKey,
                    Token: resp.SessionToken,
                    Expiration: convert_timestamp(resp.Expiration),
                    LastUpdated: "2009-11-23T00:00:00Z".to_string(),
                };
                rouille::Response::json(&creds)
            },
            _ => rouille::Response::empty_404(),
        )
    });
}

#[cfg(test)]
mod tests {
    use crate::convert_timestamp;

    #[test]
    fn timestamp_parses_properly() {
        assert_eq!(convert_timestamp(0 as f32), "1970-01-01T00:00:00Z");

        assert_eq!(convert_timestamp(1695441165 as f32), "2023-09-23T03:52:32Z");
    }
}
