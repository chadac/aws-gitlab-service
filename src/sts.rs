use error_chain::error_chain;
use serde::Deserialize;

error_chain! {
    foreign_links {
        Io(std::io::Error);
        HttpRequest(reqwest::Error);
        Fmt(rouille::url::ParseError);
    }
}

#[derive(Deserialize)]
#[allow(non_snake_case)]
pub struct Credentials {
    pub AccessKeyId: String,
    pub SecretAccessKey: String,
    pub SessionToken: String,
    pub Expiration: f32,
}

#[derive(Deserialize)]
#[allow(non_snake_case)]
struct AssumeRoleWithWebIdentityResult {
    Credentials: Credentials,
}

#[derive(Deserialize)]
#[allow(non_snake_case)]
struct AssumeRoleWithWebIdentityResponse {
    AssumeRoleWithWebIdentityResult: AssumeRoleWithWebIdentityResult,
}

#[derive(Deserialize)]
#[allow(non_snake_case)]
struct STSAssumeRoleWithWebIdentity {
    AssumeRoleWithWebIdentityResponse: AssumeRoleWithWebIdentityResponse,
}

const STS_ENDPOINT: &str = "https://sts.amazonaws.com";

pub fn assume_role_with_web_identity(
    role_arn: &str,
    token: &str,
    role_session_name: &str,
    duration: Option<i32>,
) -> Result<Credentials> {
    let params = [
        ("Version", "2011-06-15"),
        ("Action", "AssumeRoleWithWebIdentity"),
        ("DurationSeconds", &duration.unwrap_or(3600).to_string()),
        ("RoleSessionName", role_session_name),
        ("RoleArn", role_arn),
        ("WebIdentityToken", token),
    ];
    let url = reqwest::Url::parse_with_params(STS_ENDPOINT, &params)?;
    println!("{}", url);
    let client = reqwest::blocking::Client::new();
    let resp = client
        .get(url)
        .header(reqwest::header::ACCEPT, "application/json")
        .send()?;
    let contents: String = resp.text()?;

    match serde_json::from_str::<STSAssumeRoleWithWebIdentity>(&contents) {
        Ok(data) => Ok(data
            .AssumeRoleWithWebIdentityResponse
            .AssumeRoleWithWebIdentityResult
            .Credentials),
        _ => Err(contents)?,
    }
}
